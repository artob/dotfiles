alias git-reset='git reset --hard'
alias git-ls='git log --pretty=oneline'
alias git-undo='git reset --soft HEAD^'
